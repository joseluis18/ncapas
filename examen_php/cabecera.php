<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <title>Biblioteca</title>
    <meta name="viewport" content="width=device-width, initial-scale=2.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Inserte Debajo de esta linea, el codigo que permite obtener Bootstrap desde un CDN-->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

        <link rel="stylesheet" type="text/css" href="img/estilos.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css">

        <script src="//cdn.optimizely.com/js/239294537.js"></script>
    <style>
      body {
        padding-top: 60px;
      }
    </style>

  </head>

  <body>
    
    <?php
    include("menu.php");
    ?>